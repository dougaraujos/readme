<!--- Add the project logo --->
<div align="center">
    <img alt="OnlyDiamonds" title="#OnlyDiamonds" src="./assets/logo.png" />

<!--- Add the project title --->
<h1 align="center">
    <a href="#">Only Diamonds</a>
</h1>

<!--- Describe the project purpose --->
<h3 align="center">
    A fan finance platform for content creators
</h3>

<!--- Define the current status --->
<h4 align="center"> 
	 Status: 🚧 work in progress
</h4>

<!--- Table of contents --->
<p align="center">
 <a href="#-about">About</a> •
 <a href="#%EF%B8%8F-features">Features</a> •
 <a href="#-layout">Layout</a> • 
 <a href="#-how-it-works">How it works</a> • 
 <a href="#%EF%B8%8F-tech-stack">Tech Stack</a> • 
 <a href="#%EF%B8%8F-license">License</a>
</p>
</div>

&nbsp;

<!--- Add a project description --->
## 📝 About

**OnlyDiamonds** is a way to connect content creator with their fans to receive funding.
Project developed in partnership with [Diamond Brazil](https://www.diamondbrazil.com/).

&nbsp;


<!--- Describe the main features --->
## ⚙️ Features

Modelos e assinantes podem:
  - [ ] se cadastrar na plataforma
  - [ ] gerenciar seu perfil
  - [ ] visualizar, curtir e compartilhar postagem de modelos
  - [ ] enviar mensagens privadas via chat

Assinantes podem:
  - [ ] comprar conteúdos exclusivos postados por modelos


Modelos podem:
  - [ ] postar conteúdos pagos e gratuitos
  - [ ] criar álbuns de conteúdos

&nbsp;


<!--- Add the project layouts --->
## 🎨 Layout

The application layout is available on https://www.smartinnovation.com.br/onlydiamonds

&nbsp;


<!--- Describe requirements, environments and steps to run it--->
## 🚀 How it works

This project is the client-side application and need the following applications running to work:
1. [Backend]()
2. [Database]()


### Environments

| Environments | URL |
| ------------- | ------------- |
| Local | http://localhost:3000 |
| Homologation | https:// |
| Production | https:// |


### CIA Triad
-  Confidentiality
    - 🚧 WIP
-  Integrity
    - 🚧 WIP
-  Availability
    - 🚧 WIP


### Pre-requisites

Before you begin, you will need to have the following tools installed on your machine:
-  [Git](https://git-scm.com)
-  [Node.js 14+](https://nodejs.org/en/)


### Running the application
There are two ways to run the application:

#### 1. Locally
```bash
# Clone this repository
$ git clone git@gitlab.com:dougaraujos/readme.git

# Access the project folder cmd/terminal
$ cd readme

# Install the dependencies
$ yarn install

# Run the application in development mode
$ yarn dev

# The server will start at port: 3000 - go to http://localhost:3000
```

#### 2. Using Docker
```bash
# Describe all steps here
```


### Tests
🚧 WIP


### Deploying
The application is deployed following the steps below:

| Step | Goal | Pipeline |
| ------------- | ------------- | ------------- |
| 1º |  |
| 2º |  |  |


&nbsp;


<!--- Describe the tech stack used to develop it --->
## ⚒️ Tech Stack

The following tools were used in the construction of the project:

#### **Application**  ([React](https://reactjs.org/)  +  [Redux](https://redux.js.org/)  +  [Next.js](https://nextjs.org/))
-   **[React Icons](https://react-icons.github.io/react-icons/)**
-   **[React Redux](https://react-redux.js.org/)**
-   **[Styled Components](https://styled-components.com/)**

> See all dependencies in [package.json](/package.json)

#### [](https://github.com/tgmarinho/Ecoleta#utilit%C3%A1rios)**Development Tools**
-   **[ESLint](https://eslint.org/)**
-   **[React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)**
-   **[Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=pt-BR)**
-   **[TypeScript](https://www.typescriptlang.org/)**

#### [](https://github.com/tgmarinho/Ecoleta#utilit%C3%A1rios)**Utils**

-   API:  **[IBGE API](https://servicodados.ibge.gov.br/api/docs/localidades?versao=1)**  →  **[API de UFs](https://servicodados.ibge.gov.br/api/docs/localidades?versao=1#api-UFs-estadosGet)**,  **[API de Municípios](https://servicodados.ibge.gov.br/api/docs/localidades?versao=1#api-Municipios-estadosUFMunicipiosGet)**
-   API Documentation:  **[Swagger Editor](http://editor.swagger.io/)**
-   API Test:  **[Insomnia](https://insomnia.rest/)**
-   Fonts:  **[Red Hat Display](https://fonts.google.com/specimen/Red+Hat+Display)**, **[Roboto](https://fonts.google.com/specimen/Roboto)**

&nbsp;


<!--- Describe the project license --->
## ©️ License

This project is under the license [MIT](./LICENSE).

Made by [Smart Innovation](http://smartinnovation.com.br/)
